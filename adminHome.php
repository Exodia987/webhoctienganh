<?php
session_start();
if (!isset($_SESSION['IDAdmin'])) {
    header("Location: index.php");
    exit();
}
require 'dbConfig.php';
$sql = "SELECT FullName FROM admin WHERE Email ='".$_SESSION['Email']."'";
$rs = $mysqli->query($sql);
$row = $rs->fetch_row();
$adminName = $row[0];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>4Beginner</title>
    <link href="Image/hi.png" rel="icon" type="image/ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/md5.js"></script>
	<script src="js/changePass.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <style>
        fieldset.scheduler-border {
            border: 1px groove #ddd !important;
            padding: 0 1.4em 1.4em 1.4em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow: 0px 0px 0px 0px #555555;
            box-shadow: 0px 0px 0px 0px #555555;
        }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width: auto;
            padding: 0 10px;
            border-bottom: none;
        }

        .well {
            opacity: 0.8;
        }
    </style>
</head>
<body>
<?php include "head.php" ?>
<div class="container-fluid main-container">
    <div class="row">
        <div class="col-sm-6 col-sm-push-3 well">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">My Infomation</legend>
                <p>Name: <?php echo $adminName?></p>
                <p>Email: <?php echo $_SESSION['Email']?></p>
            </fieldset>
			
            <h4>Change password</h4>
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="oldPass" type="password" class="form-control" placeholder="Old password">
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="newPass" type="password" class="form-control" placeholder="New password">
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="retypePass" type="password" class="form-control" placeholder="Retype password">
                </div>
                <br>
                <div class="text-center">
                    <button class="btn btn-primary" onclick="saveChangePass()">Save change</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php include "footer.php"; ?>
    </div>
</div>
</body>
</html>