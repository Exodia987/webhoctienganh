<?php
session_start();
if (!isset($_SESSION['IDLearner'])) {
    header("Location: index.php");
    exit();
}
require 'dbConfig.php';

$IDLesson = 0;
if ($_POST['action'] == "check") {
	$percent;   //bien ghi ti le giong nhau giua 2 string
    similar_text($_POST['string1'], $_POST['string2'], $percent);
	
    $sql = 'SELECT IDLesson, Standard FROM bainoi WHERE Level = ' . $_POST['level'];
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_row();
	$IDLesson = $row[0];
	
	//neu vuot qua tieu chuan dat cua bai
    if ($row[1] <= $percent) {
        $sql = 'INSERT INTO lichsunoi(IDLearner,IDLesson,Score,Date,isPassed) ';
		$sql .= 'VALUES ('.$_SESSION["IDLearner"].','.$IDLesson.','.round($percent).',now(),t)';
        $rs = $mysqli->query($sql);
		
		//tim ra level cao nhat hien tai cua nguoi hoc (TH neu nguoi hoc dang lam lai 1 bai khac
        $sql = "SELECT min(IDLesson) FROM lichsunoi WHERE isPassed = f and ";
		$sql .= "Date in (SELECT max(Date) FROM lichsunoi WHERE IDLearner = ".$_SESSION['IDLearner']." group by IDLearner, IDLesson)";
        $rs = $mysqli->query($sql);
        $tmp = $rs->fetch_row();
        if ($tmp[0] == null) {
            $sql = 'SELECT max(IDLesson) FROM lichsunoi WHERE isPassed = t and ';
			$sql .= 'Date in (SELECT max(Date) FROM lichsunoi WHERE IDLearner =  '.$_SESSION['IDLearner'].' group by IDLearner, IDLesson)';
            $rs = $mysqli->query($sql);
            $tmp = $rs->fetch_row();
        }
		
        $sql = 'SELECT Level FROM bainoi WHERE IDLesson = '. $tmp[0];
        $rs = $mysqli->query($sql);
        $tmp = $rs->fetch_row();
		
        $levelSpeak = (int)$tmp[0] + 1;
        $sql ='UPDATE nguoihoc set SpeakLevel = '.$levelSpeak.' WHERE IDLearner = '.$_SESSION['IDLearner'];
        $rs = $mysqli->query($sql);
        $result = array(
            'percent' => round($percent),
            'isPassed' => true,
            'next' => $levelSpeak

        );
    }
	//neu khong vuot qua tieu chuan dat cua bai
	else {
        $sql = 'INSERT INTO lichsunoi(IDLearner,IDLesson,Score,Date,isPassed) ';
		$sql .= 'VALUES ('.$_SESSION['IDLearner'].','.$IDLesson.','.round($percent).',now(),f)';
        $rs = $mysqli->query($sql);
        $sql ='UPDATE nguoihoc set SpeakLevel = '.$_POST['level'].' WHERE IDLearner = '.$_SESSION['IDLearner'];
        $rs = $mysqli->query($sql);
        $result = array(
            'percent' => round($percent),
            'isPassed' => false,
            'next' => $_POST['level']
        );
    }
    echo json_encode($result);
    die;
}
?>