<?php
//cac vai tro cua nguoi dung
define("ADMIN_ROLE", 1, true);
define("LEARNER_ROLE", 2, true);

//cac trang thai cua tai khoan nguoi hoc
define("NO_ACTIVE", 0, true);
define("ACTIVATED", 1, true);

//cac trang thai cua yeu cau level
define("NON_EXIST", 1, true);
define("TOO_HIGH", 2, true);
define("DONE", 3, true);
define("DOING", 4, true);
?>