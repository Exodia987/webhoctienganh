<style type="text/css">
    .footer {
        background-image: url("Image/footer-bg.png");
        height: 450px;
        padding-top: 200px;
    }
    p {
        font-family: Verdana;
    }
</style>
<div class="footer" style="width: 100%">
    <div class="col-md-4 col-sm-6">
        <div class="wow fadeInUp" data-wow-delay="0.3s">
            <p>Vu Hong Thang - 20156510</p>
        </div>
    </div>

    <div class="col-md-1 col-sm-1"></div>

    <div class="col-md-4 col-sm-5">
        <h2>Contact</h2>
        <p class="wow fadeInUp" data-wow-delay="0.6s">
            Contact me for support<br>
            Email: 4beginnervn@gmail.com<br>
            Ha Noi University of Science and Technology
        </p>
    </div>
</div>