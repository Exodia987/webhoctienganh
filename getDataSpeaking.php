<?php
session_start();
if (!isset($_SESSION['IDAdmin'])) {
    header("Location: index.php");
    exit();
}
require 'dbConfig.php';

if (isset($_POST['action'])) {
	if ($_POST['action'] == "getTotalRecords") {
		$sql = "SELECT COUNT(IDLesson) FROM bainoi";
		$result1 = $mysqli->query($sql);
		if ($result1->num_rows > 0) {
			$row = $result1->fetch_row();
			$data = array (
				"totalRecords" => $row[0]
			);
		}
		echo json_encode($data);
	}
	
	else if ($_POST['action'] == "getAllRecord") {
		$numberRecordPerPage = 5;
		$page = 1;		//1 bien so nguyen de danh dau trang
		$startFrom = 0;
		$sql2 = "";
		$result2;
		if (isset($_POST['page'])) { 
			$page = $_POST['page']; 
		}
		$startFrom = ($page - 1) * $numberRecordPerPage;
		$sql2 = "SELECT * FROM bainoi Order By IDLesson desc LIMIT $startFrom, $numberRecordPerPage";
		$result2 = $mysqli->query($sql2);
		if ($result2) {
			while ($row = $result2->fetch_assoc()) {
				$json[] = $row;
			}
			$data['dataLesson'] = $json;
			echo json_encode($data);
		}
	}
}
?>
