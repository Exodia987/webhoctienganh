<?php
session_start();
if (!isset($_SESSION['IDAdmin']) || !isset($_POST['IDLesson'])) {
    header("Location: index.php");
    exit();
}
require 'dbConfig.php';

$sql = "SELECT * FROM bainoi WHERE IDLesson = '" .$_POST['IDLesson'] . "'";
$result = $mysqli->query($sql);

if ($result) {
	$row = $result->fetch_row();
	$data = array(
		"title"      => $row[1],
		"level"      => $row[2],
		"transcript" => $row[3],
		"standard"   => $row[4],
		"content"    => $row[5]
	);
	
	echo json_encode($data);
} else {
	echo json_encode(null);
}

?>


