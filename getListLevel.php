<?php
session_start();
if (!isset($_SESSION['IDLearner'])) {
    header("Location: index.php");
    exit();
}
	
require 'dbConfig.php';
require 'constant.php';

$type;        //$type la 1 so duoc quy dinh trong constant.php de chi ra loai level bai hoc
$totalLevels = 0;
$level = -1;
$sql1 = "";
$sql2 = "";
$sql3 = "";
$sql4 = "";
$arr = [];

/* 
 * Chuan bi cac cau lenh sql tuy theo $_POST['action']
 */
if ($_POST['action'] == "getListListen") {
	$sql1 .= "SELECT Level, IsPassed FROM lichsunghe,bainghe WHERE bainghe.IDLesson = lichsunghe.IDLesson AND  Date IN ";
	$sql1 .= "(SELECT max(DATE) FROM lichsunghe WHERE IDLearner = '" . $_SESSION['IDLearner'] . "' GROUP BY IDLearner, IDLesson)";
	
	//$sql2 chua hoan thanh, doi sau khi co $totalLevels co du lieu se noi vao sau
	$sql2 .= "SELECT Level FROM bainghe WHERE Level > ";
	
	$sql3 .= "SELECT ListenLevel FROM nguoihoc WHERE IDLearner = '" . $_SESSION['IDLearner'] . "'";
	
	$sql4 .= "SELECT IDLesson FROM bainghe WHERE Level = '" . $_POST['level'] . "'";
} 
else if ($_POST['action'] == "getListSpeak") {
	$sql1 .= "SELECT Level, IsPassed FROM lichsunoi,bainoi WHERE bainoi.IDLesson = lichsunoi.IDLesson AND  Date IN ";
	$sql1 .= "(SELECT max(DATE) FROM lichsunoi WHERE IDLearner = '" . $_SESSION['IDLearner'] . "' GROUP BY IDLearner, IDLesson)";
	
	//$sql2 chua hoan thanh, doi sau khi co $totalLevels co du lieu se noi vao sau
	$sql2 .= "SELECT Level FROM bainoi WHERE Level > ";
	
	$sql3 .= "SELECT SpeakLevel FROM nguoihoc WHERE IDLearner = '" . $_SESSION['IDLearner'] . "'";
	
	$sql4 .= "SELECT IDLesson FROM bainoi WHERE Level = '" . $_POST['level'] . "'";
} else {
	header("Location: index.php");
    exit();
}
	
//tao mang chua danh sach cac muc va tieu chi "da hoan thanh" cua cac bai da tung nghe/noi
$rs = $mysqli->query($sql1);
if ($rs->num_rows != 0) {
//nap vao mang arr thong tin cac level ma nguoi hoc da lam qua
    while ($row = $rs->fetch_row()) {
        $arr[$row[0]] = $row[1];
        $totalLevels++;
     }
}
	
//them cac muc moi vao bang (cua cac bai chua tung nghe/noi) 
$sql2 .= "'" . $totalLevels ."'";
$rs = $mysqli->query($sql2);
if ($rs->num_rows > 0) {
    while ($row = $rs->fetch_row()) {
        $arr[$row[0]] = "F";
        $totalLevels++;
    }
}
	
//Truy xuat level nghe/noi hien tai cua nguoi hoc
$rs = $mysqli->query($sql3);
if ($rs->num_rows > 0) {
	$row = $rs->fetch_row();
	$level = $row[0];
	
	if (!isset($_POST['level'])) {
		$type = NON_EXIST;
	} else {
		$rs = $mysqli->query($sql4);
		$row = $rs->num_rows;
			
		if ($row == 0) {
			//Khong tim thay bai nghe/noi cua level == level chua ton tai (TH sua URL truc tiep)
			$type = NON_EXIST;
		}
		else {
			if ($level < $_POST['level']) {
			//level yeu cau vuot qua level hien tai cua nguoi hoc
				$type = TOO_HIGH;
			}
			else if ($level > $_POST['level']) {
				//level yeu cau thap hon level hien tai cua nguoi hoc
				$type = DONE;
			} else {
				$type = DOING;
			}
		}
	}
	$result = array(
		"type" => $type,
		"currentLevel" => $level,
		"levels" => $arr,
		"totalLevels" => $totalLevels
	);
	echo json_encode($result);
} else {
	//khong tim thay level - co loi voi DB
	exit;
}
	

?>