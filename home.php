<?php
session_start();
if (!isset($_SESSION['IDLearner'])) {
    header("Location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>4Beginner</title>
    <link href="Image/hi.png" rel="icon" type="image/ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/Chart.min.js"></script>
    <script src="js/md5.js"></script>
	<script src="js/changePass.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style4.css">
	<script src="js/drawLineChart.js"></script>
    <link href="css/chart.css" rel="stylesheet">
    <style>
        .well {
            opacity: 0.8;
        }
    </style>
</head>
<body>
<?php include "head.php" ?>
<div class="container-fluid main-container">
    <div class="row">
	
        <div class="col-sm-3">
            <div class="card">
                <br>
                <img src="Image/user.png" class="img-responsive" style="margin: 0 auto">
                <h2 id="learner"></h2>
                <br>
                <table>
                    <tr>
                        <td class="left">Level speaking</td>
                        <td>Level listening</td>
                    </tr>
                    <tr>
                        <td class="left" id="speakLevel">></td>
                        <td id="listenLevel"></td>
                    </tr>
                </table>
                <br>
                <button type="button" onclick="showChangePassModal()">Change password</button>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="well">
                <div class="chartWrapper">
                    <div class="chartAreaWrapper">
                        <div class="chartAreaWrapper2">
                            <canvas id="line-chartcanvas1"></canvas>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <br>
            </div>
            <div class="well">
                <div class="chartWrapper">
                    <div class="chartAreaWrapper">
                        <div class="chartAreaWrapper2">
                            <canvas id="line-chartcanvas2"></canvas>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <br>
            </div>
			
        </div>
    </div>
    <div class="row">
        <?php include "footer.php"; ?>
    </div>
</div>

<div id="passwordModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
		
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="oldPass">Current password:</label>
                    <input type="password" class="form-control" id="oldPass">
                </div>
                <div class="form-group">
                    <label for="newPass">New password:</label>
                    <input type="password" class="form-control" id="newPass">
                </div>
                <div class="form-group">
                    <label for="retypePass">Retype password:</label>
                    <input type="password" class="form-control" id="retypePass">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="saveNewPass" onclick="saveChangePass()">Save change</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>