<?php
session_start();
if (!isset($_SESSION['IDLearner'])) {
    header("Location: index.php");
    exit();
}
require 'dbConfig.php';

if ($_POST['action'] == "getTooHightLevel") {
    $sql = 'SELECT Title, Transcript, Content FROM bainoi WHERE Level = ' . $_POST['level'];
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_row();
    $obj = array(
        "Title" => $row[0],
        "Transcript" => $row[1],
        "Content" => $row[2]
    );
    echo json_encode($obj);
	
} else if ($_POST['action'] == "getSoundExercise") {
    $sql = 'SELECT Title,Transcript,Content FROM bainoi WHERE Level = ' . $_POST['level'];
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_row();
    $obj = array(
        "Title" => $row[0],
        "Transcript" => $row[1],
        "Content" => $row[2],
    );
    echo json_encode($obj);
			
} else if ($_POST['action'] == "getOldLesson") {
    $sql = 'SELECT Title,Transcript,Content,IDLesson FROM bainoi WHERE Level = ' . $_POST["level"];
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_row();

    $title = $row[0];
    $transcript = $row[1];
    $content = $row[2];
    $IDLesson = $row[3];
    $sql = 'SELECT Score,Date FROM lichsunoi WHERE Date IN (SELECT max(Date) FROM lichsunoi';
	$sql .= 'WHERE IDLearner = ' . $_SESSION['IDLearner'] . ' AND IDLesson = ' . $IDLesson . '  GROUP BY IDLearner, IDLesson)';
    $rs = $mysqli->query($sql);
    $row = $rs->fetch_row();
    $obj = array(
        "Title" => $title,
        "Transcript" => $transcript,
        "Content" => $content,
        "Score" => $row[0],
        "OldDate" => $row[1]
    );
    echo json_encode($obj);
}
?>