function showChangePassModal() {
    $('#passwordModal').modal();
}

    function saveChangePass() {
        var oldPass = $('#oldPass').val();
        var newPass = $('#newPass').val();
		var retype = $('#retypePass').val();
        if ((retype == "") || (oldPass == "") || (newPass == "")) {
			alert("Don't let inputs empty!");
		}
        else {
            if (retype != newPass) {
				alert("Retype password is wrong!");
			}
            else {
                if (newPass == oldPass) {
					alert("New password is the same as old password. Please change!");
				}
                else {
					changePassword(newPass, oldPass);
				}
            }
        }
    }
	
	function changePassword(newPass, oldPass) {
		$.ajax({
			url: 'changePass.php',
			dataType: "json",
			data: {"oldPass": CryptoJS.MD5(oldPass).toString(), "newPass": CryptoJS.MD5(newPass).toString()},
			type: 'post',
			success: function (output) {
				if (output == "success") {
					alert("Successfully!\nYou changed your password!");
					$('#passwordModal').modal('hide');
				}
				else {
					alert(output);
				}
			}
		});
	}