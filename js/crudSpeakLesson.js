var page = 1;

function manageDataSpeaking() {
	$.ajax({
		url: "getDataSpeaking.php",
		dataType: "json",
		data: {action: "getTotalRecords", page: page},
		type: 'post',
		success: function(data) {
			var totalPage = Math.ceil(data.totalRecords / 5);
			
			//hien thi pagination
			$('#pagination').twbsPagination({
				totalPages: totalPage,
				visiblePages: 3,
				onPageClick: function(event, newPage) {
					page = newPage;
					getPageData(page);
				}
			});
		}
	});
}

function getPageData(page) {
    $.ajax({
		url: 'getDataSpeaking.php',
        dataType: 'json',
        data: {action: "getAllRecord", page: page},
		type: 'post',
		success: function(data) {
			showDataRows(data.dataLesson);
		}
    });
}

function showDataRows(data) {
    var rows = '';
    $.each(data, function(key, value) {
        rows = rows + '<tr >';
        rows += '<td>' + value.Title + '</td>';
        rows += '<td>' + value.Level + '</td>';
        rows += '<td>' + value.Transcript + '</td>';
        rows += '<td>' + value.Standard + '</td>';
        rows += '<td>' + value.Content + '</td>';
        rows += '<td data-id="' + value.IDLesson + '">';
        rows += '<button class="btn btn-primary" onclick="showInfoLesson(' + value.IDLesson + ')">';
		rows += '<span class="glyphicon glyphicon-pencil" title="Edit"></span></button>';
        rows += '<button class="remove-item btn btn-danger" onclick="removeLesson(' + value.IDLesson + ')">';
		rows += '<span class="glyphicon glyphicon-trash" title="Delete"></span></button>';
        rows += '</td></tr>';
    });
    $("tbody").html(rows);
}

function createNewLesson() {
    var title = $("#createItem").find("input[name='title']").val();
    //var level = $("#createItem").find("input[name='level']").val();
    var transcript = $("#createItem").find("textarea[name='transcript']").val();
    var standard = $("#createItem").find("input[name='standard']").val();
    var content = $("#createItem").find("textarea[name='content']").val();
	alert("clicked");
	
	if ((title != '') && (transcript != '') && (standard != '') && (content != '')) {
		$.ajax({
			url: 'createSpeakingLesson.php',
			dataType: 'json',
			type: 'post',
			data: {
				title: title,
				//level: level,
				transcript: transcript,
				standard: standard,
				content: content
			},
			success: function(output) {
				/*
				$('#titleLesson').val('');
				$('#levelLesson').val('');
				$('#transcriptLesson').val('');
				$('#standardLesson').val('');
				$('#contentLesson').val('');
				$(".modal").modal('hide');
				toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000})
				getPageData(1);*/
				alert(output);
			}
		});
	} else {
		 alert('You are missing any field.')
    }
}

function showInfoLesson(IDLesson) {
    $('#id').val(IDLesson);
    $.ajax({
		url: 'getInfoSpeakingLesson.php',
        dataType: 'json',
        type: 'post',
        data: {IDLesson: IDLesson},
        success: function(data) {
			if (data != null) {
				$('#titleLesson').val(data.title);
				$('#levelLesson').val(data.level);
				$('#transcriptLesson').val(data.transcript);
				$('#standardLesson').val(data.standard);
				$('#contentLesson').val(data.content);
				$('#editItem').modal();
			} else {
				alert("Co loi xay ra voi CSDL");
			}
        }
    });
}

function removeLesson(IDLesson) {
    var c_obj = $(this).parents("tr");
    if (confirm("Are you sure you want to delete this lesson?")) {
        $.ajax({
			url: 'deleteSpeakLesson.php',
            dataType: 'json',
            type: 'post',
            data: {IDLesson: IDLesson},
            success: function(output) {
				alert(output);
				//c_obj.remove();
				//toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});
				//getPageData();
			}
        });
    }
}

function updateLesson() {
    //e.preventDefault();
    var title = $("#editItem").find("input[name='title']").val();
    var level = $("#editItem").find("input[name='level']").val();
    var transcript = $("#editItem").find("textarea[name='transcript']").val();
    var standard = $("#editItem").find("input[name='standard']").val();
    var content = $("#editItem").find("textarea[name='content']").val();
    var IDLesson = $("#id").val();

    if ((title != '') && (level != '') && (transcript != '') && (standard != '') && (content != '')) {
        $.ajax({
			url: 'updateSpeakingLesson.php',
            dataType: 'json',
            type: 'post',
            data: {
				title: title,
				level: level,
				transcript: transcript,
				standard: standard,
				content: content,
				IDLesson: IDLesson
			},
            success: function(output) {
                if (output == "Done") {
                    toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
                }else {
                    toastr.success('Item Updated Fail.', 'Fail Alert', {timeOut: 5000});
                }
				getPageData(1);
                $(".modal").modal('hide');
            }
        });
    } else {
        alert('You are missing any field.')
    }
}