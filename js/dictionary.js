function saveWord(word) {
    console.log(word);
    $.ajax({
        url: 'saveWord.php',
        dataType: "json",
        data: {"word": word},
        type: 'post',
        success: function (output) {
            alert(output);
        }
    });
}

//chuc nang nay khong con su dung duoc do api khong the truy cap duoc nua
function findWord(word) {
    var newWord = word.toLowerCase().trim();
    var url = "http://api.wordnik.com:80/v4/word.json/" + newWord;
	url += "/definitions?limit=200&includeRelated=true&useCanonical=false&includeTags=false&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"
    console.log(url);
    $.ajax({
        url: url,
        type: 'get',
        success: function(output) {
			alert("Yes");
            if (output.length != 0) {
				alert(output);
                var tmp = "";
                var mean = "";
                for (i = 0; i < output.length; i++) {
                    if (output[i].partOfSpeech != tmp) {
                        tmp = output[i].partOfSpeech;
                        mean += "<b>" + output[i].partOfSpeech + "</b><br>";
                    }
                    mean += "- " + output[i].text + "<br>";
                }
                $('#meaning').html(mean);
            }
            else {
                $('#listenWord').hide();
                $('#meaning').text('Sorry, our dictionary hasn\'t updated this word.');
                $('#saveWord').hide();
            }
			$('#word').text(newWord);
			$('#modalWord').modal();
        }
    });
}

//thiet lap de nghe tu duoc chon
function startListen(content) {
    var msg = new SpeechSynthesisUtterance();
    var voices = speechSynthesis.getVoices();
    msg.voice = voices[1]; // giọng người đọc
    msg.voiceURI = 'native';
    msg.volume = 1; // 0 đến 1
    msg.rate = 1; // 0.1 đến 10
    msg.pitch = 2; // 0 đến 2
    msg.text = content;
    msg.lang = 'en-US'
    speechSynthesis.speak(msg);
}
