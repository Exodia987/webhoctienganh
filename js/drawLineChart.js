$(document).ready(function () {
    $.ajax({
        url: "./retrieveSpeakAndListenLevel.php",
        type: "GET",
        success: function(data) {
            console.log(data);
            var speakScores = {
                speak: []
            };
            var listenScores = {
                listen: []
            };
            var len1 = data.speak.length;
            var len2 = data.listen.length;
            var titles = [];
			var ctx1 = $("#line-chartcanvas1");
            var ctx2 = $("#line-chartcanvas2");
			
			$('#speakLevel').html(data.learnerInfo.SpeakLevel);
            $('#listenLevel').html(data.learnerInfo.ListenLevel);
            $('#learner').html(data.learnerInfo.FullName);
			
			//nap thong tin cho cac mang titles, speakScores, listenScores
            for (var i = 0; i < len1; i++) {
                titles[i] = "Level "+ data.speak[i].Level + " : " + data.speak[i].Date;
                speakScores.speak.push(data.speak[i].Score);
            }
            for (var i = 0; i < len2; i++) {
                listenScores.listen.push(data.listen[i].Score);
            }

            var data1 = {
                labels: titles,
                datasets: [
                    {
                        label: "Speaking score",
                        data: speakScores.speak,
                        backgroundColor: "blue",
                        borderColor: "lightblue",
                        fill: false,
                        lineTension: 0,
                        pointRadius: 7
                    }
                ]
            };

            var data2 = {
                labels: titles,
                datasets: [
                    {
                        label: "Listening score",
                        data: listenScores.listen,
                        backgroundColor: "green",
                        borderColor: "lightgreen",
                        fill: false,
                        lineTension: 0,
                        pointRadius: 5
                    }
                ]
            };

            var options1 = {
                title: {
                    display: true,
                    position: "top",
                    text: "Speaking Result",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "bottom"
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: false
                        }
                    }]
                }
            };

            var options2 = {
                title: {
                    display: true,
                    position: "top",
                    text: "Listening Result",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "bottom"
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: false
                        }
                    }]
                }
            };

            var chart1 = new Chart(ctx1, {
                type: "line",
                data: data1,
                options: options1
            })
            var chart2 = new Chart(ctx2, {
                type: "line",
                data: data2,
                options: options2
            })
        },
        error: function (data) {
            console.log(data);
        }
    });
});