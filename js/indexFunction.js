$(document).ready(function() {

    /* Vegas - hieu ung chuyen doi man hinh zoomOut, khong co time-bar
    -----------------------------------------------------------------*/
    $(function() {
        $('body').vegas({
            slides: [
                {src: 'Image/slide3.jpg'},
                {src: 'Image/slide7.jpg'}
            ],
            timer: false,
            transition: ['zoomOut'],
        });
    });


    /* Bieu tuong Back top - Hien thi neu dat du khoang cach so voi dau trang
   ------------------------------------------------------------------------*/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
	
    /* Animate the scroll to top
	---------------------------*/
    $('.go-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 300);
    })
    $('#goAbout').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 650}, 800);
    })


    /* su dung wow
    --------------*/
    new WOW({mobile: false}).init();

});

/* 
 * Truy xuat va nap thong tin so luong bai nghe/bai noi
 */
window.onload = function() {
    $.ajax({
        url: 'getNumberOfLessons.php',
        dataType: 'json',
        data: {"action": "getlevel"},
        type: 'post',
        success: function(output) {
            $('#countSpeakLevels').text(output.countSpeak);
            $('#countListenLevels').text(output.countListen);
        }
    });
}

 /* Dong man hinh dang nhap
  * Chuyen xuong man hinh dang ki o Home: Contact section
  */
function signUp() {
    $('#close1').click();
    $('#modal1').hide();
    event.preventDefault();
    $('html, body').animate({scrollTop: 1900}, 800);
}

 /*
  * Kich hoat khi lua chon Reset Password duoc chon
  */
function resetPassword() {
    var email = $('#email1').val();
	
    if (email == '') {
		alert("Please input email to reset password!");
	} else {
        if (validateEmail(email) == true) {
            $.ajax({
                url: 'sendLinkResetPass.php',
                dataType: 'json',
                data: {"email": email},
                type: 'post',
                success: function(output) {
                    if (output == "success") {
                        alert("We will send you a email. Please click the link in it to reset your password!");
                        $('#email1').val('');
                        $('#password1').val('');
                    }
                    else {
                        alert(output);
                    }
                }
            });
        }
    }
}

/*
 * Kich hoat khi lua chon SEND INFO duoc chon
 */
function register() {
    var name = $('#name').val();
    var email = $('#email').val();
	var pass = $('#password').val();
	
	if (name == '') {
		alert("Please don't let name field empty.");
	} else if (pass == '') {
		alert("Please don't let password field empty.");
	} else if (email == '') {
		alert("Please don't let email field empty.");
	} else {
		pass = CryptoJS.MD5(pass).toString();
		
		if (validateEmail(email) == true) {
			$.ajax({
				url: 'register.php',
				dataType: "json",
				data: {"name": name, "email": email, "password": pass},
				type: 'post',
				success: function(output) {
					if (output == "success") {
						alert("Success!\nWe will send you a email. Please confirm to finish registration. Thank you!");
						$('#email').val('');
						$('#name').val('');
						$('#password').val('');
					}
					else {
						alert(output);
					}
				}
			});
		} else {
			alert("Your email has wrong style");
		}
	}
}

function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true);
    }
    alert("You have entered an invalid email address!");
    return (false);
}

function loginFunc() {
	if ($('#password1').val() == '') {
		alert("Please don't let password field empty.");
	} else if ($('#email1').val() == '') {
		alert("Please don't let email field empty.");
	} else {
		alert("Yes");
        var email = $('#email1').val();
        var password = CryptoJS.MD5($('#password1').val()).toString();
        if (validateEmail(email) == true) {
            $.ajax({
                url: 'login.php',
                dataType: "json",
                data: {"email": email, "password": password},
                type: 'post',
                success: function(output) {
					alert("OK");
                    if (output.msg == "success") {
                        if (output.role == 1) {
                            //window.location.href = "Admin.php";
							setTimeout(function(){
								location.href = "adminHome.php";
							},100);
                        }
                        else if (output.role == 2) {
                            //window.location.href = "home.php";
							setTimeout(function(){
								location.href = "home.php";
							},100);
                        }
                    }
                    else {
                        alert(output.msg);
                    }
                }
            });
        } else {
			alert("Your email has wrong style");	
		}
    }
}

function clickBegin() {
    $.ajax({
        url: 'login.php',
        dataType: "json",
        data: {"action": "checkLogin"},
        type: 'post',
        success: function(output) {
            if (output == "no_login") {
                $('#modal1').modal();
            }
            else if (output == "learner") {
				window.location = "home.php";
			}
			else if (output == "admin") {
				window.location = "adminHome.php";
			}
        }
    });
}