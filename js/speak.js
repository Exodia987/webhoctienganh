//cac trang thai cua level
const NON_EXIST = 1;
const TOO_HIGH = 2;
const DONE = 3;

var level;
var doing = 1;

function check() {
    var s1 = $("#finalSpan").text().toLowerCase();
    if (s1 != "") {
        var s2 = $("#nd").text().toLowerCase();
        s2 = s2.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
        s2 = s2.replace(/\s{2,}/g, ' ');
        $.ajax({
            url: "checkSpeak.php",
            dataType: "json",
            data: {"action": "check", "string1": s1, "string2": s2, "level": level},
            type: 'post',
            success: function (output) {
                console.log(output);
                if (output.isPassed) {
                    $('.modal-confirm .btn').css('background', '#82ce34');
                    $('.modal-confirm .icon-box').css('background', '#82ce34');
                    $('.material-icons').html('&#xE876;');
                    $('.modal-title').html('Awesome!');
                    $('#modal-btn').text('Go to next level');
                    $('#modalMark').html(output.percent + "/100");
                    $('#modal-btn').attr("href", "speak.php?level=" + output.next);
                    $('#modal-msg').html('Congratulations! You have finished level ' + level + '.');
                    //show button next + change tag in list-group if modal is closed
                }
                else {
                    $('.modal-confirm .btn').css('background', '#ef513a');
                    $('.modal-confirm .icon-box').css('background', '#ef513a');
                    $('.material-icons').html('&#xE5CD;');
                    $('.modal-title').html('Sorry!');
                    $('#modal-msg').html('Please go back and do again. Try your best.');
                    $('#modal-btn').text('OK');
                    $('#modalMark').html(output.percent + "/100");
                    $('#modal-btn').attr("href", "speak.php?level=" + output.next);
                    //clear input
                }
                $('#myModal').modal('show');
            }
        });
    }
    else alert("You can't submit empty content.")
}

function getListLevels() {
    level = getUrlParameter('level');
	if (level == undefined) {
		level = 1;
	}
	
    $.ajax({
        url: "getListLevel.php",
        dataType: "json",
        data: {action: "getListSpeak", level: level},
        type: 'post',
        success: function(output) {
			var arr = output.levels;
            var totalLevels = output.totalLevels;
            doing = output.currentLevel;
			
			//dua ra thong bao trong phan Content
			if (output.type == NON_EXIST) {
				getNonExistLevel();
			} else if (output.type == TOO_HIGH) {
				getTooHightLevel(level);
			} else if (output.type == DONE) {
				getOldLesson(level);
			} else {
				getSoundExercise(level);
			}
			
			//bo sung cac duong link den cac bai noi vao list level
            for (i = 1; i <= totalLevels; i++) {
                if (i == doing) {
                    $('#listLevels').append('<a href="speak.php?level=' + i + '" class="list-group-item list-group-item-action">\n' +
                        'Level ' + i + '\n' + '<span class="label label-primary pull-right">Doing</span>\n' +'</a>');
				} else if (arr[i] == "F") {
					$('#listLevels').append('<a href="speak.php?level=' + i + 
					'" class="list-group-item list-group-item-action disabled">Level ' + i + '</a>');
				} else {
                    $('#listLevels').append('<a href="speak.php?level=' + i + '" class="list-group-item list-group-item-action">\n Level ' + i + 
						'\n' + '<span class="label pull-right"><img src="Image/checked.png" class="img-responsive"></span>\n' + '</a>');
				}
            }
        }
    });
}

function getNonExistLevel() {
    $('.content').replaceWith('<div class="content">' + '<table><tr>' + '<td><img src="Image/sorry.gif" alt=""></td>' +
        '<td><div style="font-size: larger; font-style: italic; font-weight: bold; font-family:\'Courier New\'">' +
        'Sorry!<br>Our system doesn\'t have this lesson. We will update soon. Let keep supporting us.<br>Thank you!' +
        '</div></td></tr></table></div>');
}

function getTooHightLevel(level) {
    $.ajax({
        url: "iniSpeak.php",
        dataType: "json",
        data: {action: "getTooHightLevel", level: level},
        type: 'post',
        success: function(output) {
            var titleLesson = "Level " + level + " - " + output.Title;
            $('#title').html(titleLesson);
            $('#needToLearn').replaceWith('<div id = "needToLearn">'+output.Content+'</div>');
            $('#nd').text(output.Transcript);
            $('#ktra').hide();
            $('#announce').replaceWith('<div id="announce"><div class="alert alert-warning" role="alert">' +
                '<img src="Image/Warning.png" style="float: left">' +
                '<div style="font-size: larger; color: red; font-weight: bold">' + 
				'You just can do this exercise if you finish all of your previous levels.</div>' +
                '</div></div>');
        }
    });
}

function getOldLesson(level) {
    $.ajax({
        url: "iniSpeak.php",
        dataType: "json",
        data: {action: "getOldLesson", level: level},
        type: 'post',
        success: function(output) {
            var title = "Level " + level + " - " + output.TieuDe;
            $('#title').html(title);
            $('#needToLearn').replaceWith('<div id = "needToLearn">' + output.Content + '</div>');
            $('#nd').text(output.Transcript);
            $('#ktra').hide();
            $('#announce').replaceWith('<div id="announce" class="alert alert-success"><table width="100%"><tr>' +
                '<td rowspan="2"><img src="Image/badge.png" class="img-responsive"></td>' +
                '<td class="text-center" style="font-size: larger">Congrat! You have passed this level at ' + output.OldDate + '</td>' +
                '</tr><tr><td>' +
                '<div class="progress" style="width: 100%">\n' +
                '<div class="progress-bar progress-bar-striped active" role="progressbar"\n' +
                '  aria-valuenow="' + output.Score + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + output.Score + '%">\n' +
                output.Score + '%\n' +
                '  </div></div></td></tr></table></div><button name="doagain" id="redo" class="btn" onclick="redo()">Redo</button>');
        }
    });
}

function getSoundExercise(level) {
    $.ajax({
        url: "iniSpeak.php",
        dataType: "json",
        data: {action: "getSoundExercise", level: level},
        type: 'post',
        success: function(output) {
            var title = "Level " + level + " - " + output.Title;
            $('#title').html(title);
            $('#needToLearn').replaceWith('<div id = "needToLearn">' + output.Content + '</div>');
            $('#nd').text(output.Transcript);
        }
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function redo() {
    $('#ktra').show();
    $('#announce').hide();
    $('#redo').hide();
}