$(document).ready(function () {
    $("#myInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#wordList li").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        $('.nors').show();
    });
});

function getListWords() {
    $.ajax({
        url: "iniWord.php",
        dataType: "json",
        data: {action: "getListWords"},
        type: 'post',
        success: function(output) {
            for (i = 0; i < output.length; i++) {
                $('#wordList').append(addWord(output[i], i));
            }
        }
    });
}

function addWord(word, index) {
    return ('<li class="list-group-item borderless" id="w' + index + '"><span class="word">' + word + '</span>' +
        '<div class="pull-right"><a onclick="see(\'' + word + '\')"><span class="glyphicon glyphicon-eye-open"></span></a>' +
        '&emsp;<a onclick="deleteWord(\'' + word + '\', \'w '+ index +'\')"><span class="glyphicon glyphicon-trash"></span></a></div></li>');
}

function see(word) {
    findWord(word);
    $('#saveWord').hide();
}

function deleteWord(word, id) {
    $.ajax({
        url: "iniWord.php",
        dataType: "json",
        data: {action: "deleteWord", word: word},
        type: 'post',
        success: function(output) {
            if (output == "success") {
                alert("Delete successfully!");
                $('#'+id).remove();
            }
            else {
                alert("Can not delete this word!");
            }
        }
    });
}

function clickYesOption() {
    var word = $('#myInput').val();
    findWord(word);
}

function clickNoOption() {
    location.reload();
}

function doSaveWord(word) {
    $.ajax({
        url: 'saveWord.php',
        dataType: "json",
        data: {"word": word},
        type: 'post',
        success: function(output) {
            alert(output);
            getListWords();
            $('.nors').hide();
            $('#myInput').val('')
        }
    });
}

function clickCloseBtn() {
    location.reload();
}