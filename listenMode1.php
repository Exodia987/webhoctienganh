<!DOCTYPE html>
<?php
session_start();
if (!isset($_SESSION["IDLearner"])) {
    header("Location: index.php");
    exit();
}
if (!isset($_GET['inputLevel'])) {
    header("Location: chooseLevelListen.php");
    exit();
}
$level = $_GET['inputLevel'];

require 'dbConfig.php';

$sql = "select * from bainghe where Level = '" . $level . "'";
$result = $mysqli->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_row();
	
    $audioName = $row[1];
    $audioPath = $row[3];
    $transcript = $row[4];
	$hiddenWords = $row[6];
	
    $_SESSION['standard'] = $row[5];
	$_SESSION['levelNext'] = $level;
}
	
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>4Beginner</title>
    <link href="Image/hi.png" rel="icon" type="image/ico">
    <!-- Bootstrap CSS + jQuery library -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style1.css">
    <link rel="stylesheet" href="css/stylePlayer.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styleListenMode1.css">
    <script src="js/jquery.js"></script>
    <script src="js/listen.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<?php include "head.php" ?>
<div class="container-fluid main-container">
    <div class="row">
        <div class="col-md-3">
            <div class="listpanel">
                <div class="title-list">List listening levels</div>
                <div class="list-group list-group-flush" id="listLevels">

                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="nd">
                <?php include "player.php"; ?>
                <br/><br/>
                <div class="exercise">
                    <form action="showResult.php" method="post">
                        <p class="guide">Let fill the blanks:</p><br/>
                        <div class="fillBlanks">
                            <p id="paragraph" class="transcript" readonly="readonly"></p>
                        </div>
                        <div class="wrapper">
                            <a href="chooseMode.php?level=<?php echo $level?>" class="btn btn-default">Go back</a>
                            <input type="submit" class="btn btn-primary" value="Submit" name="Submit" onclick="checkWords()"/>
                        </div>
                        <input type="text" hidden name="transcript" value="<?php echo $transcript ?>">
                        <input hidden id="totalWords" name="total-words" type="text" value=""/>
                        <input hidden id="listenResult" name="listen-result" type="text" value=""/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php include "footer.php"; ?>
    </div>
</div>

<?php include "modalWord.php" ?>
</body>
</html>

<script type="text/javascript">
    var audioLink = "<?php echo $audioPath ?>";
    var transcript = "<?php echo $transcript ?>";
    var audioName = "<?php echo $audioName ?>";
    var hiddenWords = "<?php echo $hiddenWords ?>";
</script>

<script type="text/javascript" src="js/player.js"></script>
<script type="text/javascript" src="js/listenMode1.js"></script>
<script>
    window.onload = function () {
        getListLevels();
        $('#volumeSlider').hide();
    }

    $(document).contextmenu(function () {
        return false;
    });

    $("body").mousedown(function (event) {
        if (event.which == 3) {
            var s = window.getSelection();
            s.modify('extend', 'backward', 'word');
            var b = s.toString();
            s.modify('extend', 'forward', 'word');
            var a = s.toString();
            s.modify('move', 'forward', 'character');
            if (b == '') findWord(a);
            else alert("If you want to search dictionary, you can't choose more than one word.");
        }
    });
</script>
</body>
</html>