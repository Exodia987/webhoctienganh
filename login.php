<?php
session_start();

require 'dbConfig.php';
require 'constant.php';

/*
 * Xu ly yeu cau tu Index: indexFunction: clickBegin()
 */
if ($_POST['action'] == "checkLogin") {
    if (isset($_SESSION['IDLearner'])) {
        echo json_encode("learner");
        exit();
    }
    if (isset($_SESSION['IDAdmin'])) {
        echo json_encode("admin");
        exit();
    }
	
    echo json_encode("no_login");
    exit();
}

/*
 * Xu ly yeu cau dang nhap tu Index: indexFunction: loginFunc()
 */
else if (isset($_POST['email']) && isset($_POST['password'])) {
    $sql = "select Password,Role from dangnhap where Email = '" . $_POST['email'] . "'";
    $rs = $mysqli->query($sql);
	$arr;
	
    if ($rs->num_rows == 0) {
        $arr = array('msg' => "This account doesn't exist in our system!");
    } else {
        $row = $rs->fetch_row();
        if ($row[0] == $_POST['password']) {
			
            if ($row[1] == ADMIN_ROLE) {
				
				//Tai khoan cua admin
                $sql = "select IDAdmin from admin where Email = '" . $_POST['email'] . "'";
                $rs = $mysqli->query($sql);
                $row2 = $rs->fetch_row();
				
                $_SESSION['IDAdmin'] = $row2[0];
                $_SESSION['Email'] = $_POST['email'];
                $arr = array('msg' => "success", 'role' => ADMIN_ROLE);
            } else {
				
				//Tai khoan cua nguoi hoc
                $sql = "select IDLearner,IsActivated from nguoihoc where Email = '" . $_POST['email'] . "'";
                $rs = $mysqli->query($sql);
                $row2 = $rs->fetch_row();
                    
                if ($row2[1] == NO_ACTIVE) {
                    $arr = array('msg' => "Your account hasn't been actived. Please check your email to finish your registration.");
                } else {
                    $_SESSION['IDLearner'] = $row2[0];
                    $_SESSION['Email'] = $_POST['email'];
                    $arr = array('msg' => "success", 'role' => LEARNER_ROLE);
                }
            }
        } else {
            //Sai password
            $arr = array('msg' => 'Password is incorrect!');
        }
    }
    echo json_encode($arr);
}
?>