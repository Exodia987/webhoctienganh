<?php
session_start();
if (!isset($_SESSION['Email'])) {
    header('Location: index.php');
    exit;
}

if (isset($_SESSION["MaNH"])) {
    require 'dbConfig.php';
    $sql = "Update nguoihoc set LastestLogout = now() where IDLearner =".$_SESSION['IDLearner'];
    $rs = $mysqli->query($sql);
    if ($rs) {
        session_destroy();
        header('Location: index.php');
    }
    else {
		header('Location: home.php');
	}
}
else {
    session_destroy();
    header('Location: index.php');
}
?>