<?php
session_start();
if (!isset($_SESSION['IDAdmin'])) {
    header("Location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>4Beginner</title>
    <link href="Image/hi.png" rel="icon" type="image/ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/toastr.min.css">
    <style>
        .modal {
            margin-top: 50px;
        }
        table {
            background-color: rgba(227, 227, 227, 0.5);
            border-collapse: collapse;
        }

        table.table-bordered{
            border:1px solid black;
        }
        table.table-bordered > thead > tr > th{
            border:1px solid black;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid black;
        }

        #pagination {
            margin: 0 auto;
        }

        .title {
            text-align: center;
        }
    </style>
	<script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/twbsPagination.min.js"></script>
    <script type="text/javascript" src="js/validator.min.js"></script>
    <script type="text/javascript" src="js/toastr.min.js"></script>
	<script type="text/javascript" src="js/crudSpeakLesson.js"></script>
</head>
<body>
<?php include "head.php" ?>
<div class="container-fluid main-container">
    <div class="row">
        <br>
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createItem" >Add new level</button>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
	
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="10%" class="title">Title</th>
                    <th width="2%" class="title">Level</th>
                    <th width="30%" class="title">Transcript</th>
                    <th width="2%" class="title">Standard score</th>
                    <th width="41%" class="title">Content</th>
                    <th class="title">Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="text-center">
                <ul id="pagination" class="pagination-md"></ul>
            </div>
            <br>
        </div>
		
    </div>
    <div class="row">
        <?php include "footer.php"; ?>
    </div>
</div>

<div class="modal fade" id="createItem" tabindex="-1" role="dialog" aria-labelledby="addItem">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
		
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="addItem">Add new level</h4>
            </div>
			
            <div class="modal-body">
			
                <div class="form-group">
                    <label class="control-label" for="title">Title:</label>
                    <input type="text" name="title" id="title" class="form-control" data-error="Nhập tiêu đề." required />
                    <div class="help-block with-errors"></div>
                </div>
				
                <div class="form-group">
                    <label class="control-label" for="transcript">Transcript:</label>
                    <textarea name="transcript" id="transcript" class="form-control" data-error="Nhập transcript." required ></textarea>
                    <div class="help-block with-errors"></div>
                </div>
				
                <div class="form-group">
                    <label class="control-label" for="standard">Standard score:</label>
                    <input type="text" name="standard" id="standard" class="form-control" data-error="Nhập tiêu chuẩn." required />
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="content">Content:</label>
                    <textarea name="content" id="content" class="form-control" data-error="Nhập nội dung." required ></textarea>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group text-center">
                    <input type="button" id="submitCreateLesson" value="Submit" class="btn btn-primary" onclick="createNewLesson()">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Level</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id" class="edit-id">
                <div class="form-group">
                    <label class="control-label" for="title">Title:</label>
                    <input type="text" id="titleLesson" name="title" class="form-control" data-error="Nhập tiêu đề." required />
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="level">Level:</label>
                    <input type="text" id="levelLesson" name="level" class="form-control" data-error="Nhập mức." readonly />
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="transcript">Transcript:</label>
                    <textarea id="transcriptLesson" name="transcript" class="form-control" data-error="Nhập transcript." required ></textarea>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="standard">Standard score:</label>
                    <input type="text" id="standardLesson" name="standard" class="form-control" data-error="Nhập tiêu chuẩn."  required />
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="content">Content:</label>
                    <textarea id="contentLesson" name="content" class="form-control" data-error="Nhập nội dung." required ></textarea>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" onclick="updateLesson()">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script>
    window.onload = function() {
        manageDataSpeaking();
    }
</script>
