<?php
/*
 * truy xuat level nghe va noi cua nguoi hoc
 */
session_start();
if (!isset($_SESSION['IDLearner'])) {
    header("Location: index.php");
    exit();
}

header('Content-Type: application/json');
require 'dbConfig.php';

$query = "SELECT Date,Score,Level FROM lichsunoi,bainoi 
          WHERE lichsunoi.IDLesson = bainoi.IDLesson AND IDLearner = '".$_SESSION['IDLearner']."' ORDER BY Date";
$result = $mysqli->query($query);
$dataSpeakHistory = array();
foreach ($result as $row) {
    $dataSpeakHistory[] = $row;
}

$query = "SELECT Date,Score,Level FROM lichsunghe,bainghe 
          WHERE lichsunghe.IDLesson = bainghe.IDLesson AND IDLearner = '".$_SESSION['IDLearner']."' ORDER BY Date";
$result = $mysqli->query($query);
$dataListenHistory = array();
foreach ($result as $row) {
    $dataListenHistory[] = $row;
}

$query = "SELECT FullName, SpeakLevel, ListenLevel FROM nguoihoc WHERE IDLearner = '".$_SESSION['IDLearner'] . "'";
$result = $mysqli->query($query);
$learnerInfo = array();
foreach ($result as $row) {
    $learnerInfo[] = $row;
}

$result->close();
$mysqli->close();
$data = array('speak' => $dataSpeakHistory, 'listen' => $dataListenHistory, 'learnerInfo' => $learnerInfo[0]);
print json_encode($data);