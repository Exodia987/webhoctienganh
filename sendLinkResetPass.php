<?php
require 'constant.php';
require 'dbConfig.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (isset($_POST['email'])) {
    $sqlQuery = "select Role from dangnhap where Email = '" . $_POST['email'] . "'";
    $rs = $mysqli->query($sqlQuery);
	
	/* Neu ton tai tai khoan thi kiem tra Role
	 * roi gui mail chua link de thay doi tai khoan
	 */
    if ($rs->num_rows > 0) {
        $row = $rs->fetch_row();
        $role = $row[0];
        $isAccountActivated = true;
		
        if ($role == LEARNER_ROLE) {
			//kiem tra tai khoan cua nguoi hoc da kich hoat hay chua
            $sqlQuery = "select IsActivated from nguoihoc where Email = '".$_POST['email']."'";
            $rs = $mysqli->query($sqlQuery);
            $rows = $rs->num_rows;
            $row = $rs->fetch_row();
            if ($row[0] == NOT_ACTIVATE) {
                $isAccountActivated = false;
                $result = "Your email hasn't been actived. Please check email to finish activation.";
            }
        }
		
		//gui mail co duong link de reset password
        if ($isAccountActivated == true) {
            $hash = urlencode("email=" . $_POST['email']);
            $link = "http://localhost/Project/resetPass.php?" . $hash;
            require 'PHPMailer/PHPMailer/src/PHPMailer.php';
            require 'PHPMailer/PHPMailer/src/SMTP.php';
            require 'PHPMailer/PHPMailer/src/Exception.php';
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
			
            try {
                //Server settings
                $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = "smtp.gmail.com";  					  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = "4beginnervn@gmail.com";            // SMTP username
                $mail->Password = "4beginner123*";                    // SMTP password
                $mail->SMTPSecure = "tls";                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to
				
                //Recipients
                $mail->setFrom("4beginnervn@gmail.com");
                $mail->addAddress($_POST['email']);     			  // Add a recipient
                $mail->Subject = "Reset password";
                $mail->Body = "We received a request to reset your password.";
				$mail->Body .= "If you sent, please click this link to reset your password: " . $link;
				$mail->Body .= " If not, you can ignore this email. Thank you, 4Beginner Team";
				
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
				
				//xu li neu gui mail thanh cong hoac khong thanh cong
                if (!$mail->send()) {
                    $result = "We can't send mail for you. Please try again!";
                    exit();
                } else {
                    $result = "success";
                }
            } catch (Exception $e) {
                $result = "We can't send mail for you. Please try again! ".$e->getMessage();
            }
        }
    } 
	
	/* Xu li neu khong ton tai tai khoan co email tuong ung
	 */
	else {
        $result = "Your email doesn't exist in our website!";
    }
	
    echo json_encode($result);
}
?>